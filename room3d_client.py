import argparse
import grpc
import time

from proto import room3d_pb2
from proto import room3d_pb2_grpc
from proto import grpc_utils

def Upload(path, stub):
    file_chunks = grpc_utils.get_file_chunks(path)
    response = stub.upload(file_chunks)
    return response


def CheckProcess(key, stub):
    msg = room3d_pb2.KeyMessage(file_key=key)
    response = stub.checkProcess(msg)
    return response


def Download(key, path, stub):
    msg = room3d_pb2.KeyMessage(file_key=key)
    response = stub.download(msg)
    grpc_utils.save_chunks(response, path)
      

def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--ip', default='localhost', type=str, help='ip address')
    parser.add_argument('--port', default=39995, type=int, help='port num')
    parser.add_argument('--path', default='./samples/image.png')
    parser.add_argument('--output', default='client.pcd', help='output path', type=str)
    args = parser.parse_args()
    return args


def main():
    args = parse()
    address = args.ip + ':' + str(args.port)
    options = [('grpc.max_message_length', grpc_utils.get_chunk_size())]
    
    channel = grpc.insecure_channel(address, options=options)
    stub = room3d_pb2_grpc.room3dStub(channel)
    
    print(f'uploading file: {args.path}')
    response = Upload(args.path, stub)
    status = response.status
    key = response.file_key
    
    print(f'upload status: {status}')
    print(f'key: {key}')

    while True:
        stat = CheckProcess(key, stub)
        print(f'current msg: {stat.msg}')
        print(f'current status: {stat.status}')
        time.sleep(10)

        if stat.status == 8:
            break

    Download(key, args.output, stub)
    print(f'downloaded at {args.output}')


if __name__ == '__main__':
    main()
