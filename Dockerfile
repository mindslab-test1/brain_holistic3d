FROM docker.maum.ai:443/brain/vision:v0.2.0-cu101-torch160

ENV path=/root/room3d

COPY ./requirements.txt ${path}/requirements.txt

WORKDIR ${path}

RUN python -m pip --no-cache-dir install -r ${path}/requirements.txt

COPY . ${path}

RUN python -m pip --no-cache-dir install --upgrade grpcio grpcio-tools protobuf && \
    python -m grpc.tools.protoc -I. --python_out . --grpc_python_out . ./proto/room3d.proto && \
    ldconfig && \
    rm -rf /tmp/* /workspace/*

EXPOSE 39995

RUN mkdir ${path}/ckpt

# RUN wget -q --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1aieMd61b-3BoOeTRv2pKu9yTk5zEinn0' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1aieMd61b-3BoOeTRv2pKu9yTk5zEinn0" -O ${path}/ckpt/resnet50_rnn__st3d.pth && rm -rf /tmp/cookies.txt
RUN wget -q --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1Xv009QOnp86wLuIHJdiR_6NgwlbiwURD' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1Xv009QOnp86wLuIHJdiR_6NgwlbiwURD" -O ${path}/ckpt/my_best_valid.pth && rm -rf /tmp/cookies.txt

ENTRYPOINT ["python", "room3d_server.py"]
