from . import room3d_pb2

CHUNK_SIZE = 1 * 1024 * 1024

def get_file_chunks(path):
    with open(path, 'rb') as f:
        while True:
            piece = f.read(CHUNK_SIZE)
            if len(piece) == 0:
                return
            chunk = room3d_pb2.FileChunk()
            chunk.file_bytes = piece
            yield chunk



def save_chunks(chunks, path, logger=None):
    if logger is not None:
        logger.info(f'Saving file to path {path}')

    with open(path, 'wb') as f:
        for chunk in chunks:
            f.write(chunk.file_bytes)
    
    status = 1
    msg = 'Successfully Saved'
    return status, msg



def get_chunk_size():
    return CHUNK_SIZE
