# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: proto/room3d.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='proto/room3d.proto',
  package='maum.brain.holistic3d',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x12proto/room3d.proto\x12\x15maum.brain.holistic3d\"/\n\tFileChunk\x12\x12\n\nfile_bytes\x18\x01 \x01(\x0c\x12\x0e\n\x06status\x18\x02 \x01(\x05\"=\n\x0cUploadStatus\x12\x0e\n\x06status\x18\x01 \x01(\x05\x12\x10\n\x08\x66ile_key\x18\x02 \x01(\t\x12\x0b\n\x03msg\x18\x03 \x01(\t\"\x1e\n\nKeyMessage\x12\x10\n\x08\x66ile_key\x18\x01 \x01(\t\",\n\rProcessStatus\x12\x0e\n\x06status\x18\x01 \x01(\x05\x12\x0b\n\x03msg\x18\x02 \x01(\t\"\x1e\n\x08InputImg\x12\x12\n\nImageBytes\x18\x01 \x01(\x0c\x32\x8d\x02\n\x06room3d\x12S\n\x06upload\x12 .maum.brain.holistic3d.FileChunk\x1a#.maum.brain.holistic3d.UploadStatus\"\x00(\x01\x12Y\n\x0c\x63heckProcess\x12!.maum.brain.holistic3d.KeyMessage\x1a$.maum.brain.holistic3d.ProcessStatus\"\x00\x12S\n\x08\x64ownload\x12!.maum.brain.holistic3d.KeyMessage\x1a .maum.brain.holistic3d.FileChunk\"\x00\x30\x01\x62\x06proto3'
)




_FILECHUNK = _descriptor.Descriptor(
  name='FileChunk',
  full_name='maum.brain.holistic3d.FileChunk',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='file_bytes', full_name='maum.brain.holistic3d.FileChunk.file_bytes', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='status', full_name='maum.brain.holistic3d.FileChunk.status', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=45,
  serialized_end=92,
)


_UPLOADSTATUS = _descriptor.Descriptor(
  name='UploadStatus',
  full_name='maum.brain.holistic3d.UploadStatus',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='status', full_name='maum.brain.holistic3d.UploadStatus.status', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='file_key', full_name='maum.brain.holistic3d.UploadStatus.file_key', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='msg', full_name='maum.brain.holistic3d.UploadStatus.msg', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=94,
  serialized_end=155,
)


_KEYMESSAGE = _descriptor.Descriptor(
  name='KeyMessage',
  full_name='maum.brain.holistic3d.KeyMessage',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='file_key', full_name='maum.brain.holistic3d.KeyMessage.file_key', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=157,
  serialized_end=187,
)


_PROCESSSTATUS = _descriptor.Descriptor(
  name='ProcessStatus',
  full_name='maum.brain.holistic3d.ProcessStatus',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='status', full_name='maum.brain.holistic3d.ProcessStatus.status', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='msg', full_name='maum.brain.holistic3d.ProcessStatus.msg', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=189,
  serialized_end=233,
)


_INPUTIMG = _descriptor.Descriptor(
  name='InputImg',
  full_name='maum.brain.holistic3d.InputImg',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='ImageBytes', full_name='maum.brain.holistic3d.InputImg.ImageBytes', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=235,
  serialized_end=265,
)

DESCRIPTOR.message_types_by_name['FileChunk'] = _FILECHUNK
DESCRIPTOR.message_types_by_name['UploadStatus'] = _UPLOADSTATUS
DESCRIPTOR.message_types_by_name['KeyMessage'] = _KEYMESSAGE
DESCRIPTOR.message_types_by_name['ProcessStatus'] = _PROCESSSTATUS
DESCRIPTOR.message_types_by_name['InputImg'] = _INPUTIMG
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

FileChunk = _reflection.GeneratedProtocolMessageType('FileChunk', (_message.Message,), {
  'DESCRIPTOR' : _FILECHUNK,
  '__module__' : 'proto.room3d_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.holistic3d.FileChunk)
  })
_sym_db.RegisterMessage(FileChunk)

UploadStatus = _reflection.GeneratedProtocolMessageType('UploadStatus', (_message.Message,), {
  'DESCRIPTOR' : _UPLOADSTATUS,
  '__module__' : 'proto.room3d_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.holistic3d.UploadStatus)
  })
_sym_db.RegisterMessage(UploadStatus)

KeyMessage = _reflection.GeneratedProtocolMessageType('KeyMessage', (_message.Message,), {
  'DESCRIPTOR' : _KEYMESSAGE,
  '__module__' : 'proto.room3d_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.holistic3d.KeyMessage)
  })
_sym_db.RegisterMessage(KeyMessage)

ProcessStatus = _reflection.GeneratedProtocolMessageType('ProcessStatus', (_message.Message,), {
  'DESCRIPTOR' : _PROCESSSTATUS,
  '__module__' : 'proto.room3d_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.holistic3d.ProcessStatus)
  })
_sym_db.RegisterMessage(ProcessStatus)

InputImg = _reflection.GeneratedProtocolMessageType('InputImg', (_message.Message,), {
  'DESCRIPTOR' : _INPUTIMG,
  '__module__' : 'proto.room3d_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.holistic3d.InputImg)
  })
_sym_db.RegisterMessage(InputImg)



_ROOM3D = _descriptor.ServiceDescriptor(
  name='room3d',
  full_name='maum.brain.holistic3d.room3d',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=268,
  serialized_end=537,
  methods=[
  _descriptor.MethodDescriptor(
    name='upload',
    full_name='maum.brain.holistic3d.room3d.upload',
    index=0,
    containing_service=None,
    input_type=_FILECHUNK,
    output_type=_UPLOADSTATUS,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='checkProcess',
    full_name='maum.brain.holistic3d.room3d.checkProcess',
    index=1,
    containing_service=None,
    input_type=_KEYMESSAGE,
    output_type=_PROCESSSTATUS,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='download',
    full_name='maum.brain.holistic3d.room3d.download',
    index=2,
    containing_service=None,
    input_type=_KEYMESSAGE,
    output_type=_FILECHUNK,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_ROOM3D)

DESCRIPTOR.services_by_name['room3d'] = _ROOM3D

# @@protoc_insertion_point(module_scope)
