import glob
import json
import numpy as np
from omegaconf import OmegaConf
import os
from PIL import Image
from tqdm import tqdm
from scipy.ndimage.filters import maximum_filter
from shapely.geometry import Polygon

import torch
import torch.utils.data as data

from model import HorizonNet
from dataset import visualize_a_data, InferDataset
from challenge.challenge import *
from misc import post_proc, utils


def find_N_peaks(signal, r=29, min_v=0.05, N=None):
    max_v = maximum_filter(signal, size=r, mode='wrap')
    pk_loc = np.where(max_v == signal)[0]
    pk_loc = pk_loc[signal[pk_loc] > min_v]
    if N is not None:
        order = np.argsort(-signal[pk_loc])
        pk_loc = pk_loc[order[:N]]
        pk_loc = pk_loc[np.argsort(pk_loc)]
    return pk_loc, signal[pk_loc]


def augment(x_img, flip, rotate):
    x_img = x_img.numpy()
    aug_type = ['']
    x_imgs_augmented = [x_img]
    if flip:
        aug_type.append('flip')
        x_imgs_augmented.append(np.flip(x_img, axis=-1))
    for shift_p in rotate:
        shift = int(round(shift_p * x_img.shape[-1]))
        aug_type.append('rotate %d' % shift)
        x_imgs_augmented.append(np.roll(x_img, shift, axis=-1))
    return torch.as_tensor(np.concatenate(x_imgs_augmented, 0), dtype=torch.float), aug_type


def augment_undo(x_imgs_augmented, aug_type):
    x_imgs_augmented = x_imgs_augmented.cpu().numpy()
    sz = x_imgs_augmented.shape[0] // len(aug_type)
    x_imgs = []
    for i, aug in enumerate(aug_type):
        x_img = x_imgs_augmented[i * sz: (i + 1) * sz]
        if aug == 'flip':
            x_imgs.append(np.flip(x_img, axis=-1))
        elif aug.startswith('rotate'):
            shift = int(aug.split()[-1])
            x_imgs.append(np.roll(x_img, -shift, axis=-1))
        elif aug == '':
            x_imgs.append(x_img)
        else:
            raise NotImplementedError()

    return np.array(x_imgs)


def inference(net, x, device, flip=False, rotate=[], visualize=False,
              force_cuboid=True, min_v=None, r=0.05):
    '''
    net   : the trained HorizonNet
    x     : tensor in shape [1, 3, 512, 1024]
    flip  : fliping testing augmentation
    rotate: horizontal rotation testing augmentation
    '''

    H, W = tuple(x.shape[2:])

    # Network feedforward (with testing augmentation)
    x, aug_type = augment(x, flip, rotate)
    y_bon_, y_cor_ = net(x.to(device))
    y_bon_ = augment_undo(y_bon_.cpu(), aug_type).mean(0)
    y_cor_ = augment_undo(torch.sigmoid(y_cor_).cpu(), aug_type).mean(0)

    # Visualize raw model output
    if visualize:
        vis_out = visualize_a_data(
            x[0],
            torch.as_tensor(y_bon_[0], dtype=torch.float),
            torch.as_tensor(y_cor_[0], dtype=torch.float))
    else:
        vis_out = None

    y_bon_ = (y_bon_[0] / np.pi + 0.5) * H - 0.5
    y_cor_ = y_cor_[0, 0]

    # Init floor/ceil plane
    z0 = 50
    _, z1 = post_proc.np_refine_by_fix_z(*y_bon_, z0)

    # Detech wall-wall peaks
    if min_v is None:
        min_v = 0 if force_cuboid else 0.05
    r = int(round(W * r / 2))
    N = 4 if force_cuboid else None
    xs_ = find_N_peaks(y_cor_, r=r, min_v=min_v, N=N)[0]
    # Generate wall-walls
    cor, xy_cor = post_proc.gen_ww(xs_, y_bon_[0], z0, tol=abs(0.16 * z1 / 1.6), force_cuboid=force_cuboid)
    if not force_cuboid:
        # Check valid (for fear self-intersection)
        xy2d = np.zeros((len(xy_cor), 2), np.float32)
        for i in range(len(xy_cor)):
            xy2d[i, xy_cor[i]['type']] = xy_cor[i]['val']
            xy2d[i, xy_cor[i - 1]['type']] = xy_cor[i - 1]['val']
        if len(xy2d) <= 2 or not Polygon(xy2d).is_valid:
            '''print(
                'Fail to generate valid general layout!! '
                    'Generate cuboid as fallback.',
                file=sys.stderr)'''
            xs_ = find_N_peaks(y_cor_, r=r, min_v=0, N=4)[0]
            cor, xy_cor = post_proc.gen_ww(xs_, y_bon_[0], z0, tol=abs(0.16 * z1 / 1.6), force_cuboid=True)

    # Expand with btn coory
    cor = np.hstack([cor, post_proc.infer_coory(cor[:, 1], z1 - z0, z0)[:, None]])

    # Collect corner position in equirectangular
    cor_id = np.zeros((len(cor) * 2, 2), np.float32)
    for j in range(len(cor)):
        cor_id[j * 2] = cor[j, 0], cor[j, 1]
        cor_id[j * 2 + 1] = cor[j, 0], cor[j, 2]

    # Normalized to [0, 1]
    cor_id[:, 0] /= W
    cor_id[:, 1] /= H

    return cor_id, z0, z1, vis_out


class infer():
    def __init__(self, conf):
        super(infer, self).__init__()
        self.conf = conf

        self.check_configs()
        # Loaded trained model
        self.net = utils.load_trained_model(HorizonNet, conf.pth).to(self.device)
        self.net.eval()

    def check_configs(self):
        conf = self.conf
        self.load_images()

        # Check target directory
        if not os.path.isdir(conf.output_dir):
            print('Output directory %s not existed. Create one.' % conf.output_dir)
            os.makedirs(conf.output_dir)
        self.device = torch.device('cpu' if conf.no_cuda else 'cuda')

    # prepare image to be processed
    def load_images(self):
        self.paths = sorted(glob.glob(self.conf.img_glob))
        if len(self.paths) == 0:
            print('no images found')
        for path in self.paths:
            assert os.path.isfile(path), '%s not found ' % path

    # load image
    def load_image(self, path):
        img_pil = Image.open(path)
        if img_pil.size != (1024, 512):
            img_pil = img_pil.resize((1024, 512), Image.BICUBIC)
        img_ori = np.asarray(img_pil.copy())[..., :3].transpose((2, 0, 1))
        x = torch.FloatTensor([img_ori / 255])
        return x

    # input image augmentation
    def process(self, x):
        flip = False
        rotate = []
        x, aug_type = augment(x, flip, rotate)
        y_bon, y_cor = self.net(x.to(self.device))
        y_bon_ = augment_undo(y_bon.clone().cpu(), aug_type).mean(0)
        y_cor_ = augment_undo(torch.sigmoid(y_cor.clone()).cpu(), aug_type).mean(0)

        return x, y_bon_, y_cor_

    def find_peaks(self, bon, cor):
        H, W = self.H, self.W
        bon_ = (bon[0].copy() / np.pi + 0.5) * H - 0.5
        cor_ = cor[0, 0].copy()

        # Init floor/ceil plane
        self.z0 = 50
        _, self.z1 = post_proc.np_refine_by_fix_z(*bon_, self.z0)

        force_cuboid = False
        min_v = 0.8
        r = 0.04
        # Detech wall-wall peaks
        r = int(round(W * r / 5))
        N = 4 if force_cuboid else None
        xs = find_N_peaks(cor_, r=r, min_v=min_v, N=N)[0]

        if len(xs) < 4:
            xs = find_N_peaks(cor_, r=r, min_v=0, N=4)[0]

        return xs

    # detect from 3d image
    def detect1(self, img, bon, cor):
        H, W = self.H, self.W
        bon_ = (bon[0].copy() / np.pi + 0.5) * 512
        cor_ = cor[0, 0].copy()

        # detects 3d edges
        def detect3d(bon_, cor_):
            H, W = self.H, self.W
            ceils = bon_[0]
            floors = bon_[1]

            real_h = 3.
            cam_h = 1.6

            # converts img (x, y) to 3D (x, y, z)
            # calculates wall-floor boundar
            angle = np.arange(W) * 2 * np.pi / W
            angle_floor = (H - floors) * np.pi / H
            angle_ceil = ceils * np.pi / H
            dist_floor = cam_h * np.tan(angle_floor)
            dist = dist_floor
            x = dist * np.cos(angle)
            y = dist * np.sin(angle)
            x = np.expand_dims(x, axis=1)
            y = np.expand_dims(y, axis=1)

            ps = np.concatenate((x, y), axis=1)  # 1024 * 2

            jump = 1
            r = 3
            # detecting edges and points
            p1 = np.roll(ps, r, axis=0)
            p2 = ps
            p3 = np.roll(ps, -r, axis=0)
            v1 = p2 - p1
            v1 /= np.linalg.norm(v1, axis=1, keepdims=True)
            v2 = p2 - p3
            v2 /= np.linalg.norm(v2, axis=1, keepdims=True)

            dists1 = np.linalg.norm(p2, axis=1) - np.linalg.norm(p1, axis=1)
            dists2 = np.linalg.norm(p2, axis=1) - np.linalg.norm(p3, axis=1)

            angles = np.einsum('ij,ij->i', v1, v2)

            i = 0
            outs = []
            while i < W:
                angle = angles[i]
                dist1 = dists1[i]
                dist2 = dists2[i]
                if cor_[i] > 0.5 and (abs(angle) < 0.1 or (abs(angle) < 0.5 and abs(dist1) + abs(dist2) > 0.05)):
                    outs.append((i, bon_[0, i]))
                    outs.append((i, bon_[1, i]))
                    i += jump
                i = i + 1
            outs = np.asarray(outs)
            return outs

        outs = detect3d(bon_, cor_)

        outs_new = []
        for i in range(len(outs) // 2):
            p1 = outs[(2 * i - 2) % len(outs)]
            p3 = outs[2 * i]
            x1, yc1 = p1
            x3, yc3 = p3

            if (x3 - x1) % W > 3:
                outs_new.append(p3)
                p4 = outs[2 * i + 1]
                outs_new.append(p4)

        outs_new = np.asarray(outs_new)

        for point in outs:
            color_point(img, point, color=COLORS['YELLOW'], r=5)
            color_point(img, point, color=COLORS['YELLOW'], r=5)
        for point in outs_new:
            color_point(img, point, color=COLORS['ORANGE'], r=4)
            color_point(img, point, color=COLORS['ORANGE'], r=4)

        return outs_new

    def detect1_batch(self, bons, cors):
        H, W = self.H, self.W

        bons_ = (bons.copy() / np.pi + 0.5) * 512
        ceils = bons_[:, 0, :]
        floors = bons_[:, 1, :]

        real_h = 3.
        cam_h = 1.6

        angle = np.arange(W) * 2 * np.pi / W
        angle_floor = (H - floors) * np.pi / H
        angle_ceil = ceils * np.pi / H
        dist_floor = cam_h * np.tan(angle_floor)
        dist_ceil = (real_h - cam_h) * np.tan(angle_ceil)
        dist = dist_floor

        x = dist * np.cos(angle)
        y = dist * np.sin(angle)
        x = np.expand_dims(x, axis=1)
        y = np.expand_dims(y, axis=1)

        ps = np.concatenate((x, y), axis=1)  # 64 * 2 * 1024

        r = 3
        p1 = np.roll(ps, r, axis=2)
        p2 = ps
        p3 = np.roll(ps, -r, axis=2)
        v1 = p2 - p1
        v1 /= np.linalg.norm(v1, axis=1, keepdims=True)
        v2 = p2 - p3
        v2 /= np.linalg.norm(v2, axis=1, keepdims=True)

        dists1 = np.linalg.norm(p2, axis=1) - np.linalg.norm(p1, axis=1)
        dists2 = np.linalg.norm(p2, axis=1) - np.linalg.norm(p3, axis=1)

        v1_ = v1.transpose((0, 2, 1)).reshape((-1, 2))
        v2_ = v2.transpose((0, 2, 1)).reshape((-1, 2))
        angles = np.einsum('ij,ij->i', v1_, v2_).reshape((-1, W))

        outs = []
        jump = 1
        for i in range(self.conf.batch_size):
            out = []
            j = 0
            while j < W:
                angle = angles[i, j]
                p = cors[i, :, j]
                dist1 = dists1[i, j]
                dist2 = dists2[i, j]
                if p > 0.5 and (abs(angle) < 0.1 or (abs(angle) < 0.5 and abs(dist1) + abs(dist2) > 0.05)):
                    out.append((j, bons_[i, 0, j]))
                    out.append((j, bons_[i, 1, j]))
                    j += jump
                j += 1
            out = np.asarray(out)
            outs.append(out)
        return outs

    # detect from 2d image
    def detect2(self, img, bon, xs):
        H, W = self.H, self.W
        points = []
        i = 0
        jump = 1
        window_inc = 3
        window_r = 8
        bon_ = (bon[0].copy() / np.pi + 0.5) * H
        xs_ = xs.copy()
        while i < W:
            inc1 = (bon_[0, i] - bon_[0, (i + window_inc) % W]) / window_inc
            inc2 = (bon_[0, i - window_inc] - bon_[0, i]) / window_inc
            inc_min = min(abs(inc1), abs(inc2)) + 1
            inc_max = max(abs(inc1), abs(inc2)) + 1

            if inc_max > 4 and inc_max / inc_min > 2:
                # set window
                if i < window_r:
                    vals0 = np.concatenate((bon_[0, (i - window_r + W):], bon_[0, :(i + window_r)]), axis=0)
                    vals1 = np.concatenate((bon_[1, (i - window_r + W):], bon_[1, :(i + window_r)]), axis=0)
                elif i + window_r > W:
                    vals0 = np.concatenate((bon_[0, (i - window_r):], bon_[0, :(i + window_r) % W]), axis=0)
                    vals1 = np.concatenate((bon_[1, (i - window_r):], bon_[1, :(i + window_r) % W]), axis=0)
                else:
                    vals0 = bon_[0, (i - window_r):(i + window_r)]
                    vals1 = bon_[1, (i - window_r):(i + window_r)]

                values0, counts0 = np.unique(vals0, return_counts=True)
                values1, counts1 = np.unique(vals1, return_counts=True)

                # detect highest/lowest points
                def find(values, counts):
                    sc1 = np.argsort(counts)[::-1]
                    sc2 = np.sort(counts)[::-1]
                    sc3 = sc1[sc2 >= 2]
                    if len(sc3) > 5:
                        sc3 = sc3[:5]
                    if len(sc3) == 0:
                        return -1, -1
                    up = values[np.max(sc3)]
                    down = values[np.min(sc3)]

                    return up, down

                c0, c1 = find(values0, counts0)
                c2, c3 = find(values1, counts1)

                if (c0, c1) == (-1, -1) or (c2, c3) == (-1, -1):
                    i += 1
                    continue

                # find key points near peak points
                for x in xs_:
                    if abs(x - i) < 6:  # 2 * jump
                        print('add')
                        LR = -1 if i < x else 0 if i == x else 1
                        if np.sum(np.asarray(points).reshape(-1, 2)[:, 0] == x + LR) >= 2:
                            continue

                        p = np.asarray(points).reshape(-1, 2)
                        ys = p[np.any([p[:, 0] == x, p[:, 0] == x + 1, p[:, 0] == x - 1], axis=0), 1]
                        if len(ys) != 0:
                            d0 = np.min(np.abs(ys - c0))
                            d1 = np.min(np.abs(ys - c1))
                            d2 = np.min(np.abs(ys - c2))
                            d3 = np.min(np.abs(ys - c3))
                            if np.sort((d0, d1, d2, d3))[-2] < 5:
                                continue

                        color = np.array([255, 255, 0])
                        if bon_[0, x] > bon_[0, i]:
                            color_point(img, (x + LR, c1), color, r=2)
                            points.append([x + LR, c1])
                        else:
                            color_point(img, (x + LR, c0), color, r=2)
                            points.append([x + LR, c0])
                        if bon_[1, x] > bon_[1, i]:
                            color_point(img, (x + LR, c3), color, r=2)
                            points.append([x + LR, c3])
                        else:
                            color_point(img, (x + LR, c2), color, r=2)
                            points.append([x + LR, c2])
                        break
                i += jump
            i += 1
        points = np.asarray(points)
        return points

    def detect3(self, img, bon, xs, points):
        xs_ = xs.copy()
        cor_id_out = np.zeros((2 * len(xs_), 2))
        cor_id_out[0::2, 0] = xs_
        cor_id_out[1::2, 0] = xs_

        bon_ = ((bon[0] / np.pi + 0.5) * 512)
        cor_id_out[0::2, 1] = bon_[0, xs_]
        cor_id_out[1::2, 1] = bon_[1, xs_]

        sort_ind = np.lexsort((cor_id_out[:, 1], cor_id_out[:, 0]))
        cor_id_out = cor_id_out[sort_ind]

        i = 0
        while i < len(cor_id_out):
            u0, v0 = cor_id_out[i]
            u1, v1 = cor_id_out[i + 1]
            assert u0 == u1
            if not np.any(np.isin([u0 - 1, u0, u0 + 1], points[:, 0])):
                color = np.array([255, 0, 0])
                points = np.concatenate((points, [[u0, v0]]), axis=0)
                points = np.concatenate((points, [[u1, v1]]), axis=0)
                color_point(img, (u0, v0), color)
                color_point(img, (u1, v1), color)
            i += 2
        for u, v in points:
            v = int(v)
            u = int(u)
            color_point(img, (u, v), np.array([255, 0, 255]), r=2)
        return points

    def find_hidden(self, img, bon, xs):
        H, W = self.H, self.W
        # Generate wall-walls
        force_cuboid = False
        bon_ = (bon[0].copy() / np.pi + 0.5) * H - 0.5
        cor, xy_cor = post_proc.gen_ww(xs, bon_[0], self.z0, tol=abs(0.16 * self.z1 / 1.6),
                                       force_cuboid=force_cuboid)

        cor = np.hstack([cor, post_proc.infer_coory(cor[:, 1], self.z1 - self.z0, self.z0)[:, None]])

        cor_id = np.zeros((len(cor) * 2, 2), np.float32)
        for j in range(len(cor)):
            cor_id[j * 2] = cor[j, 0], cor[j, 1]
            cor_id[j * 2 + 1] = cor[j, 0], cor[j, 2]
        for u, v in cor_id:
            v = int(v)
            u = int(u)
            color_point(img, (u, v), np.array([0, 0, 0]), r=1)

        return cor_id

    def ensemble(self, img, bon, xs, cands):
        H, W = self.H, self.W

        cands_ = []
        for cand in cands:
            if len(cand) != 0:
                cands_.append(cand)
        cands_ = np.concatenate(cands_, axis=0)

        points_final = []
        for x in xs:
            cam_h = 1.6
            angle = x / W * 2 * np.pi
            bon_ = ((bon[0] / np.pi + 0.5) * 512)
            angle_floor = (H - bon_[1, x]) / H * np.pi
            angle_ceil = bon_[0, x] / H * np.pi
            dist_floor = cam_h * np.tan(angle_ceil)

            if dist_floor < 4.5:
                window = 7
                height = 10
            else:
                window = 5
                height = 5

            cand = cands_[np.abs(cands_[:, 0] - x) < window].copy()

            if len(cand) == 0:
                continue

            m = np.mean(cand[:, 1])
            y0 = cand[np.argmin(cand[:, 1]), 1]
            c0 = (x, y0)
            y3 = cand[np.argmax(cand[:, 1]), 1]
            c3 = (x, y3)
            y1 = np.sort(cand[:, 1])[::-1][np.argmax(np.sort(cand[:, 1])[::-1] < m)]
            c1 = (x, y1)
            y2 = np.sort(cand[:, 1])[np.argmax(np.sort(cand[:, 1]) > m)]
            c2 = (x, y2)

            if y1 - y0 > height and y3 - y2 > height:
                color_point(img, c0, COLORS['GREEN'], r=2)
                color_point(img, c3, COLORS['GREEN'], r=2)
                points_final.append(c0)
                points_final.append(c3)
                color_point(img, c1, COLORS['GREEN'], r=2)
                color_point(img, c2, COLORS['GREEN'], r=2)
                points_final.append(c1)
                points_final.append(c2)
                '''elif y1 - y0 > height or y3 - y2 > height:
                if y1 - y0 > height:
                    y3 = y2 + (y1 - y0)
                    c3 = (x, y3)

                else:
                    y0 = y1 - (y3 - y2)
                    c0 = (x, y0)
                color_point(img, c0, COLORS['BLUE'], r=2)
                color_point(img, c3, COLORS['BLUE'], r=2)
                points_final.append(c0)
                points_final.append(c3)
                color_point(img, c1, COLORS['BLUE'], r=2)
                color_point(img, c2, COLORS['BLUE'], r=2)
                points_final.append(c1)
                points_final.append(c2)'''
            else:
                y0 = np.median(cand[cand[:, 1] < m, 1])
                y1 = np.median(cand[cand[:, 1] > m, 1])
                c0 = (x, y0)
                c1 = (x, y1)
                color_point(img, c0, COLORS['BLUE'], r=2)
                color_point(img, c1, COLORS['BLUE'], r=2)
                points_final.append(c0)
                points_final.append(c1)
        points_final = np.asarray(points_final)
        return points_final

    def final(self, cor):
        H, W = self.H, self.W
        bon_ceil, bon_floor = get_front_edges(cor, shape=(H, W))

        points_up = []
        for point in cor:
            u, v = point.copy()
            if abs(bon_ceil[int(u)] - v) < 1 or abs(bon_floor[int(u)] - v) < 1:
                points_up.append(point)

        detected = []
        for i in range(W):
            window = 1
            incL = (bon_ceil[i] - bon_ceil[i - window]) / window
            incR = (bon_ceil[(i + window) % W] - bon_ceil[i]) / window

            incM = max(incL, incR)
            incm = min(incL, incR)

            absM = max(abs(incL), abs(incR))
            absm = min(abs(incL), abs(incR))

            if (absM + 1) / (absm + 1) > 2:
                point = (i, bon_ceil[i])
                dists = np.linalg.norm(np.asarray(points_up) - point, axis=1)
                if np.min(dists) > 5:
                    detected.append(point)
                    detected.append((i, bon_floor[i]))

        detected = np.asarray(detected)
        points_up = np.asarray(points_up)
        if len(detected) != 0:
            points_final = np.concatenate((points_up, detected), axis=0)
        else:
            points_final = points_up
        points_final = points_final[np.lexsort((points_final[:, 1], points_final[:, 0]))]
        return points_final

    # Inferencing
    def inference(self):
        dataset = InferDataset(self.conf.img_glob, mode='test')
        loader = data.DataLoader(
            dataset, batch_size=self.conf.batch_size, shuffle=False,
            num_workers=self.conf.num_workers, pin_memory=not self.conf.no_cuda
        )
        infer_iter = iter(loader)
        with torch.no_grad():
            for step in tqdm(range(len(loader)), desc='Inferencing'):
                x, ks = next(infer_iter)
                self.H, self.W = tuple(x.shape[2:])
                H, W = self.H, self.W

                x, y_bon, y_cor = self.process(x)
                out_batch = self.detect1_batch(y_bon, y_cor)
                # 44 ms
                for i in range(len(ks)):
                    k = os.path.split(ks[i])[-1][:-4]
                    x_ = x[i].unsqueeze(0)
                    y_bon_ = np.expand_dims(y_bon[i], axis=0)
                    y_cor_ = np.expand_dims(y_cor[i], axis=0)

                    vis_out = visualize_a_data(
                        x_[0], torch.as_tensor(y_bon_[0], dtype=torch.float),
                        torch.as_tensor(y_cor_[0], dtype=torch.float)
                    )
                    vis_out = None
                    # 55 ms
                    xs = self.find_peaks(y_bon_, y_cor_)
                    # 57 ms
                    # outs_new = self.detect1(vis_out, y_bon_, y_cor_)
                    outs_new = out_batch[i]
                    # outs_new = []
                    # 125 ms
                    points = self.detect2(vis_out, y_bon_, xs)
                    points = np.asarray(points).reshape(-1, 2)
                    assert len(points) % 2 == 0
                    points = self.detect3(vis_out, y_bon_, xs, points)
                    assert len(points) % 2 == 0
                    cor_id = self.find_hidden(vis_out, y_bon_, xs)

                    cands = [points, outs_new]

                    points_final = self.ensemble(vis_out, y_bon_, xs, cands)
                    points_final = self.final(points_final)

                    img_path = os.path.join(self.conf.output_dir, k + '_ori.png')
                    # Image.fromarray(vis_out).save(img_path)

                    with open(os.path.join(self.conf.output_dir, k + '.txt'), 'w') as f:
                        for i in range(len(points_final)):
                            point = points_final[i].copy()
                            u, v = point.astype(int)
                            line = '%d %d\n' % (int(u), int(v))
                            f.write(line)

                    with open(os.path.join(self.conf.output_dir, k + '.json'), 'w') as f:
                        json.dump({
                            'z0': float(self.z0),
                            'z1': float(self.z1),
                            'uv': [
                                [float(u) if abs(u) <= 1 else float(u / W), float(v) if abs(v) <= 1 else float(v / H)]
                                for u, v in points_final],
                        }, f)

    def inference_one(self, img, path_out=None, return_hidden=True):
        MAX_SIZE = (2048, 1024)
        img_ori = Image.fromarray(np.asarray(img))

        if img_ori.size[0] > MAX_SIZE[0] or img_ori.size[1] > MAX_SIZE[1]:
            img_ori = img_ori.resize(MAX_SIZE, Image.BICUBIC)
        img = img_ori.resize((1024, 512), Image.BICUBIC)
        img_ori = np.array(img_ori)[..., :3] / 255.
        scale = (img_ori.shape[1] / 1024., img_ori.shape[0] / 512.)
        img = np.array(img)[..., :3] / 255.

        self.H, self.W = img.shape[:2]

        x = torch.FloatTensor(img.transpose([2, 0, 1]).copy()).unsqueeze(0)

        with torch.no_grad():
            H, W = self.H, self.W

            x, y_bon, y_cor = self.process(x)
            # 44 ms

            x_ = x
            y_bon_ = y_bon
            y_cor_ = y_cor

            vis_out = None
            xs = self.find_peaks(y_bon_, y_cor_)
            outs_new = self.detect1(vis_out, y_bon_, y_cor_)
            points = self.detect2(vis_out, y_bon_, xs)
            points = np.asarray(points).reshape(-1, 2)

            assert len(points) % 2 == 0
            points = self.detect3(vis_out, y_bon_, xs, points)

            assert len(points) % 2 == 0
            cor_id = self.find_hidden(vis_out, y_bon_, xs)

            cands = [points, outs_new]

            points_final = self.ensemble(vis_out, y_bon_, xs, cands)
            points_final = self.final(points_final)

            # show, skel = skeleton2(img * 255, points_final, show_hidden=True)
            points_final[:, 0] *= scale[0]
            points_final[:, 1] *= scale[1]
            show, skel = skeleton2(img_ori * 255, points_final, show_hidden=True)

            if path_out is None:
                path_out = 'output_3d.pcd'
            # o3d.io.write_point_cloud(path_out, show)
        if return_hidden:
            output = o3d.geometry.PointCloud()
            output.points = o3d.utility.Vector3dVector(np.concatenate(
                (show.points, skel.points), axis=0
            ))
            output.colors = o3d.utility.Vector3dVector(np.concatenate(
                (show.colors, skel.colors), axis=0
            ))
            return output
        else:
            return show
        # return path_out
        # return points_final

    def inference_one_ori(self, img, path_out=None, return_hidden=True):
        MAX_SIZE = (2048, 1024)
        img_ori = Image.fromarray(np.asarray(img))

        if img_ori.size[0] > MAX_SIZE[0] or img_ori.size[1] > MAX_SIZE[1]:
            img_ori = img_ori.resize(MAX_SIZE, Image.BICUBIC)
        img = img_ori.resize((1024, 512), Image.BICUBIC)
        img_ori = np.array(img_ori)[..., :3] / 255.
        scale = (img_ori.shape[1] / 1024., img_ori.shape[0] / 512.)
        img = np.array(img)[..., :3] / 255.

        self.H, self.W = img.shape[:2]

        x = torch.FloatTensor(img.transpose([2, 0, 1]).copy()).unsqueeze(0)
        with torch.no_grad():
            H, W = self.H, self.W

            cor_id, z0, z1, vis_out = inference(self.net, x, 'cuda', False, [], True, False, None, 0.05)
            cor_id_out = cor_id.copy()
            cor_id_out[:, 0] *= 1024
            cor_id_out[:, 1] = 512 * cor_id_out[:, 1]
            show, skel = skeleton2(img_ori * 255, cor_id_out, show_hidden=True)

        if return_hidden:
            output = o3d.geometry.PointCloud()
            output.points = o3d.utility.Vector3dVector(np.concatenate(
                (show.points, skel.points), axis=0
            ))
            output.colors = o3d.utility.Vector3dVector(np.concatenate(
                (show.colors, skel.colors), axis=0
            ))
        else:
            output = show

        if path_out is not None:
            o3d.io.write_point_cloud(path_out, output)

        return output
        # return path_out
        # return points_final


def main():
    path = 'config_infer.yaml'
    conf = OmegaConf.load(path)
    s = infer(conf)
    img = Image.open('./image.png')
    path_out = 'venv.pcd'
    s.inference_one_ori(img, path_out)

    return

    import sys
    # scene = 'scene_03118_11618.png'
    scene = sys.argv[1]
    draw_from_gt(scene)
    path = '/DATA2/Holistic-3D/Structured3D/train/image/%s.png' % scene[:-4]

    conf = OmegaConf.load('config_infer.yaml')
    conf.pth = './ckpt/holistic3d/1/best_valid.pth'

    s = infer(conf)
    # s.inference()
    img = Image.open(path)
    points = s.inference_one(img, path_out='%s_3d.pcd' % scene[:-4])
    conf.pth = 'ckpt/resnet50_rnn__st3d.pth'
    s = infer(conf)
    img = Image.open(path)
    s.inference_one_ori(img, path_out='%s_ori.pcd' % scene[:-4])
    print(points)


def compare():
    path_my = './ckpt/holistic3d/17/best_valid.pth'
    path_ho = './ckpt/resnet50_rnn__st3d.pth'

    scene = 'scene_03118_11618.png'

    dir_common = '/DATA2/Holistic-3D/Structured3D/train/'
    path_img = os.path.join(dir_common, 'image', scene[:-4] + '.png')
    path_label = os.path.join(dir_common, 'label', scene[:-4] + '.txt')

    conf = OmegaConf.load('config_infer.yaml')
    conf.pth = path_my
    s = infer(conf)
    img = Image.open(path_img)
    points_my = s.inference_one(img)

    conf.pth = path_ho
    s = infer(conf)
    # s.net = utils.load_trained_model(HorizonNet, path_ho).to(conf.device)
    # s.net.eval()
    img = Image.open(path_img)
    points_ho = s.inference_one(img)

    with open(path_label) as f:
        cor = np.array([line.strip().split() for line in f if line.strip()], np.float32)
        cor = np.roll(cor[:, :2], -2 * np.argmin(cor[::2, 0]), 0)
    points_gt = cor.copy()

    with open(path_img, 'rb') as f:
        img = Image.open(f)
        img = np.asarray(img)

    img_out = img.copy()[..., :3] / 255.

    from challenge import get_front_edges
    ceils_gt, floors_gt = get_front_edges(points_gt, img_out, color2=COLORS['GREEN'])
    ceils_my, floors_my = get_front_edges(points_my, img_out, color2=COLORS['BLUE'])
    ceils_ho, floors_ho = get_front_edges(points_ho, img_out, color2=COLORS['RED'])

    '''show_gt, skel_gt = skeleton2(img_out, points_gt, show_hidden=False)
    show_my, skel_my = skeleton2(img_out, points_my, show_hidden=False)
    show_ho, skel_ho = skeleton2(img_out, points_ho, show_hidden=False)

    ind_gt = np.asarray(show_gt.points)[:, 2] == 0
    ind_my = np.asarray(show_my.points)[:, 2] == 0
    ind_ho = np.asarray(show_ho.points)[:, 2] == 0

    show_my.paint_uniform_color([0, 0, 1])
    show_ho.paint_uniform_color([1, 0, 0])

    po_gt = np.asarray(show_gt.points)[ind_gt]
    po_my = np.asarray(show_my.points)[ind_my]
    po_ho = np.asarray(show_ho.points)[ind_ho]
    points = np.concatenate((po_gt, po_my, po_ho))
    colors = np.concatenate((np.asarray(show_gt.colors)[ind_gt], np.ones_like(po_my) * np.array([0, 0, 1]), np.ones_like(po_ho) * np.array([1, 0, 0])))

    out = o3d.geometry.PointCloud()
    out.points = o3d.utility.Vector3dVector(points)
    out.colors = o3d.utility.Vector3dVector(colors)
    path_output = 'output_compare.pcd'
    o3d.io.write_point_cloud(path_output, out)
    print(path_output)'''

    cam_h = 1.6
    real_h = 3.
    cam_angle = np.pi  # for future usage with lower FOV cams
    wall_points = 500
    stretch_xy = 2
    H, W, C = img_out.shape

    pcd = []
    col = []
    for i in range(len(ceils_gt)):
        c_gt = ceils_gt[i]
        f_gt = floors_gt[i]

        c_my = ceils_my[i]
        f_my = floors_my[i]

        c_ho = ceils_ho[i]
        f_ho = floors_ho[i]

        angle = i / W * 2 * np.pi
        angle_floor = (H - f_gt) / H * np.pi
        angle_ceil = c_gt / H * np.pi
        dist_floor = cam_h * np.tan(angle_floor)

        for j in range(H):
            color = np.ones(3)
            if j >= f_gt:
                ratio = (H - j) / float(H - f_gt)
                angle_ = angle_floor * ratio
                x = cam_h * np.tan(angle_) * np.cos(angle)
                y = cam_h * np.tan(angle_) * np.sin(angle)
                z = 0.
                color[1] = 0
                if j >= f_my:
                    color[2] = 0
                if j >= f_ho:
                    color[0] = 0
            elif j > c_gt:
                ratio = (j - c_gt) / (f_gt - c_gt)
                angle_ = (np.pi - angle_floor - angle_ceil) * ratio + angle_ceil
                x = dist_floor * np.cos(angle)
                y = dist_floor * np.sin(angle)
                if angle_ < np.pi / 2:
                    z = cam_h + dist_floor * np.tan(np.pi / 2. - angle_)
                else:
                    z = cam_h - dist_floor * np.tan(angle_ - np.pi / 2.)
                color = ratio * COLORS['BLACK'] + (1 - ratio) * COLORS['GRAY']
                color /= 255.
            else:
                x, y, z = 0, 0, 0

            x *= stretch_xy
            x = -x
            y *= stretch_xy

            pcd.append(np.array([x, y, z]))
            col.append(color)

    pcd = np.asarray(pcd)
    col = np.asarray(col)

    show = o3d.geometry.PointCloud()
    show.points = o3d.utility.Vector3dVector(pcd)
    show.colors = o3d.utility.Vector3dVector(col)

    path_output = 'output_compare.pcd'
    o3d.io.write_point_cloud(path_output, show)
    print(path_output)

    # Corner with minimum x should at the beginning

    # infer, change ckpt path, infer, get gt, draw bottoms


if __name__ == '__main__':
    main()
