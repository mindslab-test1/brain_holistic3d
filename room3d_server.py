import argparse
from concurrent import futures
import glob
import grpc
import io
import logging
import os
import subprocess
import time
import uuid

import open3d as o3d
from omegaconf import OmegaConf
from PIL import Image

from inference import infer

from proto import room3d_pb2
from proto import room3d_pb2_grpc
from proto import grpc_utils

CHUNK_SIZE = 2 * 1024 * 1024  # 2MB

EXT_IMAGE = '.png'
EXT_OUTPUT = '.pcd'


class room3dServer(room3d_pb2_grpc.room3dServicer):
    def __init__(self, args, logger):
        self.args = args
        self.conf = OmegaConf.load(args.path_conf)

        self.s = infer(self.conf)

        self.server_temp = './server_temp'
        os.makedirs(self.server_temp, exist_ok=True)

        self.logger = logger
        self.logger.info(f'server started at port {args.port}')

    def upload(self, request_iterator, context):
        key = str(uuid.uuid4())
        path_save = os.path.join(self.server_temp, key + EXT_IMAGE)

        self.logger.debug(f'path: {path_save}')

        status, msg = grpc_utils.save_chunks(request_iterator, path_save)
        self.logger.debug(f'saved image to path {path_save}')

        try:
            with open(path_save, 'rb') as f:
                input_bytes = f.read()

            if len(input_bytes) > 4 * 1024 * 512:
                status = 2
                msg = 'too big file!'

            img = Image.open(io.BytesIO(input_bytes))
            # if img.size[0] != 1024 or img.size[1] != 512:
            #     status = 2
            #     msg = 'wrong img size, should be 1024*512'
        except:
            status = 2
            msg = 'unknown error occurred while opening uploaded file'

        if status == 1:
            self.logger.debug(f'status ok, run infer with key {key}')
            self.infer(key)

        return room3d_pb2.UploadStatus(status=status, file_key=key, msg=msg)

    def checkProcess(self, request, context):
        file_key = request.file_key
        self.logger.debug(f'got check request with key {file_key}')

        path_file = os.path.join(self.server_temp, file_key + EXT_IMAGE)
        path_output = os.path.join(self.server_temp, file_key + EXT_OUTPUT)
        if not os.path.isfile(path_output):
            if not os.path.isfile(path_file):
                status = 10
                msg = 'File does not exist(not uploaded)'
            else:
                status = 6
                msg = 'Process is in queue'
        else:
            status = 8
            msg = 'Process completed(can download)'

        self.logger.debug(f'returning response of check request with status: {status}, msg: {msg}')
        return room3d_pb2.ProcessStatus(status=status, msg=msg)

    def download(self, request, context):
        key = request.file_key
        self.logger.debug(f'download request with key: {key}')

        path_output = os.path.join(self.server_temp, key + EXT_OUTPUT)
        return grpc_utils.get_file_chunks(path_output)

    def infer(self, key):
        self.logger.info(f'inference with key: {key}')
        path_image = os.path.join(self.server_temp, key + EXT_IMAGE)

        preprocess_scripts = [
            'python', 'preprocess.py',
            '--img_glob', path_image,
            '--output_dir', f'{self.server_temp}',
            '--rgbonly'
        ]
        preprocess_script = ' '.join(preprocess_scripts)
        subprocess.call(preprocess_script, shell=True)
        print(f'preprocess of {key} finished!')

        path_image = os.path.join(self.server_temp, key + '_preprocess' + EXT_IMAGE)
        with open(path_image, 'rb') as f:
            input_bytes = f.read()
        img = Image.open(io.BytesIO(input_bytes))

        if not self.args.ori:
            output = self.s.inference_one(img, None)
        else:
            output = self.s.inference_one_ori(img, None)

        path_output = os.path.join(self.server_temp, key+EXT_OUTPUT)
        o3d.io.write_point_cloud(path_output, output)

    def check_files(self):
        path_files = sorted(glob.glob(os.path.join(self.server_temp, '*')))

        time_now = time.time()

        for path_file in path_files:
            mtime = os.path.getmtime(path_file)
            if time_now - mtime > 24 * 60 * 60:  # deleting old files every day
                os.remove(path_file)
                self.logger.debug(f'deleted old file {path_file}')


def serve():
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', type=int, default=39995)
    parser.add_argument('--path_conf', type=str, default='./configs/config_infer.yaml', help='config omegaconf path')
    parser.add_argument('--ori', action='store_false', help='use original post processing inference code(store_false)')

    parser.add_argument('--log_level', type=str, default='INFO', help='logger level (default: INFO)')

    args = parser.parse_args()

    logging.basicConfig(level=args.log_level.upper())
    logger = logging.getLogger('Holistic3D')

    room3d_server = room3dServer(args=args, logger=logger)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1, ))
    room3d_pb2_grpc.add_room3dServicer_to_server(room3d_server, server)
    server.add_insecure_port('[::]:%d' % args.port)
    server.start()

    try:
        while True:
            room3d_server.check_files()
            time.sleep(60 * 60)  # check for deleting old files every hour
    except KeyboardInterrupt:
        room3d_server.logger(f"KeyboardInterrupt, stopping server")
        server.stop(0)


if __name__ == '__main__':
    serve()
