import numpy as np
import cv2
import os
import open3d as o3d
import glob

COLORS = {
    'RED': np.array([255, 0, 0]),
    'BLUE': np.array([0, 0, 255]),
    'GREEN': np.array([0, 255, 0]),
    'YELLOW': np.array([255, 255, 0]),
    'SKY': np.array([0, 255, 255]),
    'ORANGE': np.array([255, 128, 0]),
    'PURPLE': np.array([255, 0, 255]),
    'BLACK': np.array([0, 0, 0]),
    'WHITE': np.array([255, 255, 255]),
    'GRAY': np.array([128, 128, 128]),

}


def color_point(img, coords, color=None, r=3):
    if img is None:
        return
    H, W, C = img.shape
    u, v = coords

    x0, x1, y0, y1 = map(int, [max(v - r, 0), min(v + r, H), max(u - r, 0), min(u + r, W)])

    if color is not None:
        img[x0:x1, y0:y1, :] = color


def stewart(a, b, theta, angle):
    eps = 1e-10
    div = (a - b * np.cos(theta))
    if 0 <= div < eps:
        div += eps
    elif eps > div + eps > 0:
        div -= eps
    const = b * np.sin(theta) / div

    div = const + np.tan(angle)
    if 0 <= div < eps:
        div += eps
    elif eps > div + eps > 0:
        div -= eps
    x = a * const / div
    r = x / np.cos(angle)
    return r


def func(coords, img=None, shape=None, color=None):
    """calculates edges from corners

    img or shape should be given
    img: ndarray, (H, W, 3)
    shape: tuple, ndim >= 2
    coords: (2, 3)
    color: segment color, ndarray, uint8
    """

    eps = 1e-10

    if img is not None:
        H, W, C = img.shape
    elif shape is not None:
        H, W = shape[:2]
    else:
        raise ValueError('img or shape should be given')

    # define camera and room parameters
    # arbitrary, doesn't really matter, but for debugging
    cam_h = 1.6
    real_h = 3.  # can be calculated from cam_h, but first set manually.
    cam_angle = np.pi  # for future usage with lower FOV cams

    # parse coords
    x0, yc0, yf0 = coords[0]
    x1, yc1, yf1 = coords[1]

    swap = False
    '''if x0 - x1 > W * 2 // 4:
        x1 += W
    elif x0 > x1:
        x0, yc0, yf0 = coords[1]
        x1, yc1, yf1 = coords[0]
        swap = True
    if x1 - x0 > W * 2 // 4:
        x0, yc0, yf0 = coords[1]
        x1, yc1, yf1 = coords[0]
        x1 = x1 + W
        swap = True # 607, 1175'''

    # calculate floor, ceiling length(camera to wall distance)
    theta0u = yc0 / H * cam_angle
    dist0u = (real_h - cam_h) * np.tan(theta0u)
    theta0d = (H - yf0) / H * cam_angle
    dist0d = cam_h * np.tan(theta0d)
    # dist0u = dist0d

    theta1u = yc1 / H * cam_angle
    dist1u = (real_h - cam_h) * np.tan(theta1u)
    theta1d = (H - yf1) / H * cam_angle
    dist1d = cam_h * np.tan(theta1d)
    # dist1u = dist1d

    floors = []
    ceils = []

    for x in range(int(x0), int(x1) + 1):
        angle = (x - x0) / W * 2 * np.pi
        theta = (x1 - x0) / W * 2 * np.pi
        distu = stewart(dist0u, dist1u, theta, angle)
        distd = stewart(dist0d, dist1d, theta, angle)
        # print(distu, distd)

        thetau = np.arctan(distu / (real_h - cam_h))
        thetad = np.arctan(distd / cam_h)
        # print(thetau, thetad)
        u = thetau / np.pi * H
        d = thetad / np.pi * H

        color_point(img, (x % W, u), color, r=1)
        color_point(img, (x % W, H - d), color, r=1)

        ceils.append((x % W, u))
        floors.append((x % W, H - d))

    ceils = np.asarray(ceils).reshape(-1, 2)
    floors = np.asarray(floors).reshape(-1, 2)

    if swap:
        ceils = ceils[::-1]
        floors = floors[::-1]

    return ceils, floors


def get_gt_edges(cor, img=None, shape=None, color=None):
    '''get gt ceils, floors' y coord

    image or shape should be given.
    '''
    if img is not None:
        H, W, C = img.shape
    elif shape is not None:
        H, W = shape[:2]
    else:
        raise ValueError('img or shape should be given')
    floors = np.zeros((0, 2))
    ceils = np.zeros((0, 2))
    for i in range(len(cor) // 2):
        x0 = cor[2 * i, 0]
        yc0 = cor[2 * i, 1]
        yf0 = cor[2 * i + 1, 1]

        i_ = (i + 1) % (len(cor) // 2)
        x1 = cor[2 * i_, 0]
        yc1 = cor[2 * i_, 1]
        yf1 = cor[2 * i_ + 1, 1]
        if i == len(cor) // 2 - 1:
            x1 += W

        coords = np.array([x0, yc0, yf0, x1, yc1, yf1]).reshape(2, 3)
        ceil, floor = func(coords, img=img, shape=shape, color=color)
        ceils = np.concatenate((ceils, ceil), axis=0)
        floors = np.concatenate((floors, floor), axis=0)

    return ceils, floors


def get_front_edges(cor, img=None, shape=None, color1=None, color2=None, return_gt=False):
    if img is not None:
        H, W, C = img.shape
    elif shape is not None:
        H, W = shape[:2]
    else:
        raise ValueError('img or shape should be given')
    ceils, floors = get_gt_edges(cor, img=img, color=color2, shape=shape)

    bon_ceil = []
    bon_floor = []
    # colors frontal ceils, floors
    for i in range(W):
        ceil = np.min(ceils[ceils[:, 0].astype(int) == i, 1])
        floor = np.max(floors[floors[:, 0].astype(int) == i, 1])
        color_point(img, (i, ceil), color1, r=1)
        color_point(img, (i, floor), color1, r=1)
        bon_ceil.append(ceil)
        bon_floor.append(floor)
    bon_ceil = np.asarray(bon_ceil)
    bon_floor = np.asarray(bon_floor)

    if return_gt:
        return bon_ceil, bon_floor, ceils, floors
    else:
        return bon_ceil, bon_floor


def room2(path_img, path_label):
    img = cv2.imread(path_img)

    with open(path_label) as f:
        cor = np.array([line.strip().split() for line in f if line.strip()], np.float32)
        cor = np.roll(cor[:, :2], -2 * np.argmin(cor[::2, 0]), 0)

    # color gt points
    for point in cor:
        u, v = point.copy()
        color_point(img, (u, v), COLORS['GREEN'], r=3)

    # get gt ceils, floors' y coord
    # ceils, floors = get_gt_edges(cor, img=img, color=COLORS['RED'])

    # colors frontal ceils, floors
    bon_ceil, bon_floor = get_front_edges(cor, img=img, color1=COLORS['PURPLE'], color2=COLORS['BLUE'])

    # detect key points and draw
    # func2(img, bon_ceil, bon_floor)

    cv2.imshow('color', cv2.resize(img, (2048, 1024)))
    cv2.waitKey(0)
    cv2.destroyAllWindows()


# TODO search algorithm should be changed
def detect_diff(values):
    '''detect non-differentiable points

    values: (1024,)
    '''
    w = len(values)
    window = 5
    diff = []
    jump = 3
    i = 0
    while i < w:
        incR = (values[i] - values[(i + window) % w]) / float(window)
        incL = (values[i - window] - values[i]) / float(window)

        incM = max(incR, incL)
        incm = min(incR, incL)

        # if incR * incL < 0:
        if incM - incm > 2:
            diff.append((i, values[i]))
            i += jump
        i += 1

    diff = np.reshape(diff, (-1, 2))
    return diff


# TODO search algorithm should be changed
def detect_cont(values):
    '''detects discontinuous points

    values: float, (1024,)
    '''
    w = len(values)
    window = 1
    cont = []
    jump = 5

    i = 0
    while i < w:
        incR = (values[i] - values[(i + window) % w]) / float(window)
        incL = (values[i - window] - values[i]) / float(window)

        tanR = np.abs(incR)
        tanL = np.abs(incL)
        incM = max(tanR, tanL)
        incm = min(tanR, tanL)

        if incM > 4 and (incM + 1) / (incm + 1) > 2:
            cont.append((i, values[i]))
            cont.append((i + 1, values[i + jump]))  # values should be changed
            i += jump
            continue

        else:
            i += 1

    cont = np.reshape(cont, (-1, 2))
    return cont


def func2(img, ceils, floors):
    contC = detect_cont(ceils)
    contF = detect_cont(floors)

    for index, value in contC:
        color = np.array([255, 128, 0])
        color_point(img, (index, value), color, r=2)
    for index, value in contF:
        color = np.array([255, 128, 0])
        color_point(img, (index, value), color, r=2)

    diff = detect_diff(ceils)
    # diff = np.array([]).reshape(-1, 2)
    for index, value in diff:
        color = np.array([128, 0, 128])
        color_point(img, (index, value), color, r=2)

    points = np.concatenate((contC, contF, diff), axis=0)
    return points


def to3d(path_img, path_label, write_pcd=True):
    img = cv2.imread(path_img)
    if img.shape[2] == 4:
        img = cv2.cvtColor(img, cv2.COLOR_RGBA2RGB)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    H, W, C = img.shape

    with open(path_label) as f:
        cor = np.array([line.strip().split() for line in f if line.strip()], np.float32)
        cor = np.roll(cor[:, :2], -2 * np.argmin(cor[::2, 0]), 0)

    # color gt points
    for point in cor:
        u, v = point.copy().astype(int)
        color_point(img, (u, v), COLORS['GREEN'], r=5)

    # get gt ceils, floors' y coord
    # ceils, floors = get_gt_edges(cor, img=img, color=COLORS['RED'])
    # colors frontal ceils, floors
    bon_ceil, bon_floor, ceils, floors = get_front_edges(cor, img=img, color1=COLORS['BLACK'], color2=COLORS['BLUE'],
                                                         return_gt=True)
    # find, color frontal points
    points_up = []
    for point in cor:
        u, v = point.copy()
        if abs(bon_ceil[int(u)] - v) < 1 or abs(bon_floor[int(u)] - v) < 1:
            points_up.append(point)
            color_point(img, point, COLORS['RED'], r=5)

    # detect discontinuity
    detected = []
    for i in range(W):
        window = 1
        incL = (bon_ceil[i] - bon_ceil[i - window]) / window
        incR = (bon_ceil[(i + window) % W] - bon_ceil[i]) / window

        incM = max(incL, incR)
        incm = min(incL, incR)

        absM = max(abs(incL), abs(incR))
        absm = min(abs(incL), abs(incR))

        if (absM + 1) / (absm + 1) > 2:
            color_point(img, (i, bon_ceil[i]), COLORS['RED'], r=3)
            point = (i, bon_ceil[i])
            dists = np.linalg.norm(np.asarray(points_up) - point, axis=1)
            if np.min(dists) > 5:
                color_point(img, point, COLORS['YELLOW'], r=5)
                color_point(img, (i, bon_floor[i]), COLORS['YELLOW'], r=5)
                detected.append(point)
                detected.append((i, bon_floor[i]))

    '''cv2.imshow('img', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()'''

    show, skel = skeleton2(img, cor, show_hidden=True)

    if write_pcd:
        img = cv2.imread(path_img)
        if img.shape[2] == 4:
            img = cv2.cvtColor(img, cv2.COLOR_RGBA2RGB)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        # colors = img.reshape(-1, 3) / 255.
        # show.colors = o3d.utility.Vector3dVector(colors)
        o3d.io.write_point_cloud('output_3d.pcd', show)

    visualise = [show, skel]
    o3d.visualization.draw_geometries(visualise)


def skeleton2(img, cor, show_hidden=True):
    H, W, C = img.shape
    pcd = np.zeros((H, W, 3))

    bon_ceil, bon_floor, ceils, floors = get_front_edges(
        cor, img=img, color1=COLORS['BLACK'], color2=COLORS['BLUE'], return_gt=True)

    cam_h = 1.6
    real_h = 3.
    cam_angle = np.pi  # for future usage with lower FOV cams
    wall_points = 300
    stretch_xy = 2
    eps = 1e-10

    pcd_skel = []
    for i in range(len(ceils)):
        xc, yc = ceils[i]

        angle = xc / float(W) * 2 * np.pi
        angle_floor = ((H - floors[i, 1]) / H) * np.pi
        angle_ceil = (yc / H) * np.pi
        dist_floor = cam_h * np.tan(angle_floor)
        dist_ceil = (real_h - cam_h) * np.tan(angle_ceil)

        if int(bon_ceil[int(xc)]) == int(yc):
            coords = np.zeros((H, 3))

            f_thres = int(bon_floor[int(xc)])

            ratio_f = (H - np.arange(f_thres, H)) / (H - f_thres + eps)
            angle_f = angle_floor * ratio_f

            coords[f_thres:, 2] = 0
            coords[f_thres:, 1] = cam_h * np.tan(angle_f) * np.sin(angle)
            coords[f_thres:, 0] = cam_h * np.tan(angle_f) * np.cos(angle)

            c_thres = int(bon_ceil[int(xc)])
            ratio_c = np.arange(c_thres) / (bon_ceil[int(xc)] + eps)
            angle_c = angle_floor * ratio_c

            coords[:c_thres, 0] = cam_h * np.tan(angle_c) * np.cos(angle)
            coords[:c_thres, 1] = cam_h * np.tan(angle_c) * np.sin(angle)
            coords[:c_thres, 2] = cam_h + dist_floor * np.tan(np.pi / 2. - angle_ceil)

            ratio_w = np.arange(0, f_thres - c_thres) / (f_thres - c_thres + eps)
            angle_w = (np.pi - angle_floor - angle_ceil) * ratio_w + angle_ceil

            coords[c_thres:f_thres, 0] = dist_floor * np.cos(angle)
            coords[c_thres:f_thres, 1] = dist_floor * np.sin(angle)
            coords[c_thres:f_thres, 2] = cam_h + dist_floor * np.tan(np.pi / 2. - angle_w)

            coords[:, 0] *= stretch_xy
            coords[:, 0] *= -1
            coords[:, 1] *= stretch_xy

            pcd[:, int(xc), :] = coords

        if show_hidden:  # and int(bon_ceil[int(xc)]) != int(yc):
            x = dist_floor * np.cos(angle)
            y = dist_floor * np.sin(angle)
            x *= stretch_xy
            x = -x
            y *= stretch_xy
            z = 0
            '''for k in range(wall_points):
                # z = float(k) / wall_points * real_h
                maxz = cam_h + dist_floor * np.tan(np.pi / 2. - angle_ceil)
                z = k / wall_points * maxz'''
            pcd_skel.append(np.array([x, y, z]))

    if len(pcd_skel) == 0:
        pcd_skel = [[0, 0, 0]]
    pcd_skel = np.asarray(pcd_skel)

    xy_final = []
    xy = pcd_skel.copy()
    for i in range(len(xy)):
        p1 = xy[i]
        p2 = xy[(i + 1) % len(xy)]
        thres = 0.1
        dist = np.linalg.norm(p1 - p2)
        if dist > thres:
            add = int(100 * dist)
            for j in range(add):
                ratio = j / add
                p = ratio * p1 + (1 - ratio) * p2
                xy_final.append(p)
        else:
            xy_final.append(p1)
    xy_final = np.asarray(xy_final)

    # pcd_skel = []
    # z_scale = 0.1
    # for k in range(wall_points):
    #     pcd_skel_ = xy_final.copy()
    #     pcd_skel_[:, 2] = k / wall_points
    #     pcd_skel.append(pcd_skel_)
    # pcd_skel = np.vstack(pcd_skel)

    skel = o3d.geometry.PointCloud()
    skel.points = o3d.utility.Vector3dVector(pcd_skel)

    colors = img.reshape(-1, 3) / 255.
    points = pcd.reshape(-1, 3)
    show = o3d.geometry.PointCloud()

    show_ceiling = False
    if not show_ceiling:
        maxz = np.max(points[:, 2]) - 0.1
        ids = pcd_skel[:, 2] < maxz
        pcd_skel = pcd_skel[ids]
        skel = o3d.geometry.PointCloud()
        skel.points = o3d.utility.Vector3dVector(pcd_skel)

        ids = points[:, 2] < maxz
        points = points[ids]
        colors = img.reshape(-1, 3)[ids] / 255.

    skel.paint_uniform_color([0.8, 1, 1])

    show.points = o3d.utility.Vector3dVector(points)
    show.colors = o3d.utility.Vector3dVector(colors)

    return show, skel


# deprecated in favor of skeleton2
def skeleton(cor, img, only_hidden=False):
    H, W, C = img.shape
    wall_points = 500

    cam_h = 1.6
    real_h = 3.
    cam_angle = np.pi  # for future usage with lower FOV cams
    pcd_skel = []

    bon_ceil, bon_floor, ceils, floors = get_front_edges(cor, img=img, color1=COLORS['PURPLE'], color2=COLORS['BLUE'],
                                                         return_gt=True)

    for j in range(len(ceils)):
        xc, yc = ceils[j]
        if only_hidden and int(bon_ceil[int(xc)]) == int(yc):
            continue

        angle = xc / float(W) * 2 * np.pi
        angle_floor = ((H - floors[j, 1]) / float(H)) * np.pi
        angle_ceil = (yc / float(H)) * np.pi
        dist_floor = cam_h * np.tan(angle_floor)
        dist_ceil = (real_h - cam_h) * np.tan(angle_ceil)

        x = dist_floor * np.cos(angle)
        y = dist_floor * np.sin(angle)
        for k in range(wall_points):
            z = k / wall_points * real_h
            pcd_skel.append((x, y, z))

    pcd_skel = np.asarray(pcd_skel)

    return pcd_skel


# will remove this function
# instead use dataset.find_occlusion
def find_hidden_points(path_img, path_label):
    img = cv2.imread(path_img)
    H, W, C = img.shape

    with open(path_label) as f:
        cor = np.array([line.strip().split() for line in f if line.strip()], np.float32)
        cor = np.roll(cor[:, :2], -2 * np.argmin(cor[::2, 0]), 0)

    # color gt points
    for point in cor:
        u, v = point.copy()
        color_point(img, (u, v), np.array([0, 255, 0]), 3)

    # get gt ceils, floors' y coord
    floors = np.zeros((0, 2))
    ceils = np.zeros((0, 2))
    for i in range(len(cor) // 2):
        x0 = cor[2 * i, 0]
        yc0 = cor[2 * i, 1]
        yf0 = cor[2 * i + 1, 1]

        x1 = cor[(2 * i + 2) % len(cor), 0]
        yc1 = cor[(2 * i + 2) % len(cor), 1]
        yf1 = cor[(2 * i + 3) % len(cor), 1]

        coords = np.array([x0, yc0, yf0, x1, yc1, yf1]).reshape(2, 3)

        ceil, floor = func(coords, img=img, color=np.array([255, 255, 0]))
        ceils = np.concatenate((ceils, ceil), axis=0)
        floors = np.concatenate((floors, floor), axis=0)

    # ceils = ceils.astype(int)
    # floors = floors.astype(int)

    bon_ceil = []
    bon_floor = []
    # colors frontal ceils, floors
    for i in range(W):
        ceil = np.min(ceils[ceils[:, 0].astype(int) == i, 1])
        floor = np.max(floors[floors[:, 0].astype(int) == i, 1])
        color = np.array([64, 128, 255])
        color_point(img, (i, ceil), color, r=1)
        color_point(img, (i, floor), color, r=1)
        bon_ceil.append(ceil)
        bon_floor.append(floor)
    bon_ceil = np.asarray(bon_ceil)
    bon_floor = np.asarray(bon_floor)

    pcd = np.zeros((H, W, 3))

    cam_h = 1.6
    real_h = 3
    cam_angle = np.pi  # for future usage with lower FOV cams

    for i in range(W):
        angle = i / float(W) * 2 * np.pi
        angle_floor = ((H - bon_floor[i]) / float(H)) * np.pi
        angle_ceil = (bon_ceil[i] / float(H)) * np.pi
        dist_floor = cam_h * np.tan(angle_floor)
        dist_ceil = (real_h - cam_h) * np.tan(angle_ceil)
        # real_h *= (dist_ceil / dist_floor)

        for j in range(H):
            if j >= bon_floor[i]:
                x = dist_floor * np.cos(angle)
                y = dist_floor * np.sin(angle)
                x = x * (H - j) / (H - bon_floor[i])
                y = y * (H - j) / (H - bon_floor[i])
                z = 0.
            elif j <= bon_ceil[i]:
                x = dist_floor * np.cos(angle)
                y = dist_floor * np.sin(angle)
                x = x * j / bon_ceil[i]
                y = y * j / bon_ceil[i]
                z = real_h
            else:
                x = dist_floor * np.cos(angle)
                y = dist_floor * np.sin(angle)
                z = real_h * (bon_floor[i] - j) / (bon_floor[i] - bon_ceil[i])
            pcd[j, i] = np.array([x, y, z])

    point_xyz = np.zeros((len(cor), 3), np.float32)
    for i in range(len(cor)):
        point = cor[i]
        u, v = point.copy().astype(int)
        point_xyz[i] = pcd[v, u]

    point_xyz_new = []
    for i in range(len(cor) // 2):
        p1 = point_xyz[(2 * i - 2) % len(cor)]
        p2 = point_xyz[(2 * i) % len(cor)]
        p3 = point_xyz[(2 * i + 2) % len(cor)]

        def find_new(p1, p2, p3):
            thres = 0.3

            v1 = (p2 - p1)[:2]
            v2 = (p2 - p3)[:2]

            points_ret = []
            if np.abs(np.dot(v1, v2) / np.linalg.norm(v1) / np.linalg.norm(v2)) > thres:
                q1 = p1.copy()
                q1[:2] += np.dot(v2, v1) * v2 / np.linalg.norm(v2) ** 2
                q2 = p3.copy()
                q2[:2] += np.dot(v2, v1) * v1 / np.linalg.norm(v1) ** 2

                def get_cos(axes):
                    cos = axes[0] / np.linalg.norm(axes[:2])
                    return cos

                print(get_cos(q1), get_cos(p1), get_cos(q2), get_cos(p3))
                if get_cos(q1) > get_cos(p1):
                    print('!!!detected!!!')
                    points_ret.append(q1)
                    points_ret.append(p2)
                elif get_cos(q2) < get_cos(p3):
                    print('!!!detected!!!')
                    points_ret.append(p2)
                    points_ret.append(q2)
                else:
                    print('no hidden point???')
                    points_ret.append(p2)
            else:
                points_ret.append(p2)
            points_ret = np.asarray(points_ret)
            return points_ret

        points_ret1 = find_new(p1, p2, p3)

        p1 = point_xyz[(2 * i - 1) % len(cor)]
        p2 = point_xyz[(2 * i + 1) % len(cor)]
        p3 = point_xyz[(2 * i + 3) % len(cor)]

        points_ret2 = find_new(p1, p2, p3)

        assert len(points_ret1) == len(points_ret2)

        for j in range(len(points_ret1)):
            p1 = points_ret1[j]
            p2 = points_ret2[j]
            point_xyz_new.append(p1)
            point_xyz_new.append(p2)

    point_xyz_new = np.asarray(point_xyz_new)
    print(len(point_xyz_new))
    print(len(cor))

    cor_new = []
    for i in range(len(point_xyz_new) // 2):
        p1 = point_xyz_new[2 * i].copy()
        p2 = point_xyz_new[2 * i + 1].copy()

        x1, y1, z1 = p1
        x2, y2, z2 = p2

        assert x1 == x2 and y1 == y2

        dist = np.linalg.norm(p1[:2])
        angle = np.arccos(p1[0] / dist)
        if y1 < 0:
            angle = 2 * np.pi - angle

        print(z1, z2)
        angle_floor = np.arctan(dist / cam_h)
        angle_ceil = np.arctan(dist / (real_h - cam_h))
        x = angle * (W / (2 * np.pi))
        y1 = angle_ceil * (H / np.pi)
        y2 = H - angle_floor * (H / np.pi)

        cor_new.append((x, y1))
        cor_new.append((x, y2))
        color_point(img, (x, y1), COLORS['BLUE'], r=3)
        color_point(img, (x, y2), COLORS['BLUE'], r=3)
    cor_new = np.asarray(cor_new)

    cv2.imshow('img', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    assert False

    bon_ceil, bon_floor, ceils, floors = get_front_edges(cor_new, img=img, color1=COLORS['RED'],
                                                         color2=COLORS['ORANGE'],
                                                         return_gt=True)

    cv2.imshow('img', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    assert False

    points_new = []
    bon_ceil, bon_floor, ceils, floors = get_front_edges(cor, img=img, color1=COLORS['BLUE'], color2=COLORS['BLACK'],
                                                         return_gt=True)
    for i in range(W):
        if abs(bon_ceil[i] - bon_ceil[i - 1]) > 10:
            angle0 = i / float(W) * 2 * np.pi
            angle_floor0 = ((H - floors[i, 1]) / float(H)) * np.pi
            dist_floor0 = cam_h * np.tan(angle_floor0)

            angle_floor1 = ((H - floors[i - 1, 1]) / float(H)) * np.pi
            dist_floor1 = cam_h * np.tan(angle_floor1)

            if abs(dist_floor0 - dist_floor1) < 0.05:
                continue
            else:
                vec0 = pcd[int(bon_ceil[i]), i] - pcd[int(bon_ceil[i - 1]), i - 1]
                vec1 = pcd[int(bon_ceil[i]), i] - pcd[int(bon_ceil[(i + 1) % W]), (i + 1) % W]

    cv2.imshow('color', cv2.resize(img, (2048, 1024)))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    pcl_new = o3d.geometry.PointCloud()
    pcl_new.points = o3d.utility.Vector3dVector(point_xyz_new.reshape(-1, 3))
    pcl_new.paint_uniform_color([1, 0.706, 0])

    '''colors = img.reshape(-1, 3) / 255.
    points = pcd.reshape(-1, 3)
    show = o3d.geometry.PointCloud()
    show.points = o3d.utility.Vector3dVector(points)
    show.colors = o3d.utility.Vector3dVector(colors)
    visualise = [show, pcl_new]
    # visualise = [pcl_new]
    o3d.visualization.draw_geometries(visualise)'''


# move to utils.py
def key_points(path_img, path_label):
    img = cv2.imread(path_img)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    H, W, C = img.shape

    with open(path_label) as f:
        cor = np.array([line.strip().split() for line in f if line.strip()], np.float32)
        cor = np.roll(cor[:, :2], -2 * np.argmin(cor[::2, 0]), 0)

    # color gt points
    for point in cor:
        u, v = point.copy()
        color_point(img, (u, v), COLORS['GREEN'], r=4)

    # get gt ceils, floors' y coord
    ceils, floors = get_gt_edges(cor, img=img, color=COLORS['RED'])

    # colors frontal ceils, floors
    bon_ceil, bon_floor = get_front_edges(cor, img=img, color1=COLORS['PURPLE'], color2=COLORS['BLUE'])
    bon = np.vstack((bon_ceil, bon_floor))

    points_up = []
    for point in cor:
        u, v = point.copy()
        if abs(bon_ceil[int(u)] - v) < 1 or abs(bon_floor[int(u)] - v) < 1:
            points_up.append(point)
            color_point(img, point, COLORS['RED'], r=5)

    detected = []
    for i in range(W):
        window = 1
        incL = (bon_ceil[i] - bon_ceil[i - window]) / window
        incR = (bon_ceil[(i + window) % W] - bon_ceil[i]) / window

        incM = max(incL, incR)
        incm = min(incL, incR)

        absM = max(abs(incL), abs(incR))
        absm = min(abs(incL), abs(incR))

        if (absM + 1) / (absm + 1) > 2:
            color_point(img, (i, bon_ceil[i]), COLORS['RED'], r=3)
            point = (i, bon_ceil[i])
            dists = np.linalg.norm(np.asarray(points_up) - point, axis=1)
            if np.min(dists) > 5:
                color_point(img, point, COLORS['YELLOW'], r=5)
                color_point(img, (i, bon_floor[i]), COLORS['YELLOW'], r=5)
                detected.append(point)
                detected.append((i, bon_floor[i]))

    detected = np.asarray(detected)
    points_up = np.asarray(points_up)

    if len(detected) != 0:
        points_final = np.concatenate((points_up, detected), axis=0)
    else:
        points_final = points_up
    for point in points_final:
        color_point(img, point, COLORS['BLACK'], r=2)

    # return img, points_final

    cv2.imshow('color', cv2.resize(img, (1536, 768)))
    cv2.waitKey(0)
    cv2.destroyAllWindows()


# move to utils.py
def new_gt():
    '''
    generates challenge gt data(visible layout)
    '''
    from PIL import Image
    from tqdm import tqdm
    path = '/DATA2/Holistic-3D/Structured3D/train'
    paths_img = sorted(glob.glob(os.path.join(path, 'image', '*.*')))
    paths_label = sorted(glob.glob(os.path.join(path, 'label', '*.*')))

    out_path = './test/St3d'
    os.makedirs(os.path.join(out_path, 'image'), exist_ok=True)
    os.makedirs(os.path.join(out_path, 'label'), exist_ok=True)
    for i in tqdm(range(20040, len(paths_img))):
        path_img = paths_img[i]
        path_label = paths_label[i]

        img, points_final = key_points(path_img, path_label)
        points_final = points_final[np.lexsort((points_final[:, 1], points_final[:, 0]))]
        get_gt_edges(points_final, img, color=COLORS['GRAY'])
        out_img = os.path.join(out_path, 'image', os.path.split(path_img)[-1])
        Image.fromarray(img).save(out_img)
        out_label = os.path.join(out_path, 'label', os.path.split(path_label)[-1])
        with open(out_label, 'w') as f:
            for j in range(len(points_final)):
                point = points_final[j].copy()
                u, v = point
                line = '%d %d\n' % (int(u), int(v))
                f.write(line)


# metrics
def pixel_error():
    import glob
    from tqdm import tqdm
    gtpath = '/DATA2/Holistic-3D/Structured3D/train/label/'
    # gtpath = 'test/St3d/label'
    # dtpath = '../infer/ablation2/ho2_full'
    # dtpath = '../infer/ablation2/my2_full'
    dtpath = '../infer/ablation2/my2_no3'

    print(gtpath)
    print(dtpath)

    gtpaths = sorted(glob.glob(os.path.join(gtpath, '*.txt')))
    if len(gtpaths) == 21727:
        gtpaths = gtpaths[20040:]
    dtpaths = sorted(glob.glob(os.path.join(dtpath, '*.txt')))

    assert len(gtpaths) == len(dtpaths)
    scores = []

    for i in tqdm(range(len(gtpaths))):
        gt_path = gtpaths[i]
        dt_path = dtpaths[i]
        with open(gt_path) as f:
            gt_cor = np.array([line.strip().split() for line in f if line.strip()], np.float32)
            gt_cor = np.roll(gt_cor[:, :2], -2 * np.argmin(gt_cor[::2, 0]), 0)
        with open(dt_path) as f:
            dt_cor = np.array([line.strip().split() for line in f if line.strip()], np.float32)
            dt_cor = np.roll(dt_cor[:, :2], -2 * np.argmin(dt_cor[::2, 0]), 0)

        gt_ceil, gt_floor = get_front_edges(gt_cor, shape=(512, 1024))
        dt_ceil, dt_floor = get_front_edges(dt_cor, shape=(512, 1024))

        accs = []
        for j in range(1024):
            gc = gt_ceil[j]
            gf = gt_floor[j]
            dc = dt_ceil[j]
            df = dt_floor[j]

            acc = np.min((gc, dc)) + np.min((512 - gf, 512 - df)) + 512 - np.max((gc, dc)) - np.max(
                (512 - gf, 512 - df))
            accs.append(acc)

        score = np.mean(accs) / 512.
        scores.append(score)
    print('final %', np.mean(scores) * 100)


# metrics
def corner_error():
    import glob
    from sklearn.metrics.pairwise import euclidean_distances
    gtpath = '/DATA2/Holistic-3D/Structured3D/train/label/'
    # gtpath = 'test/St3d/label'
    # dtpath = '../infer/ablation2/ho2_full'
    # dtpath = '../infer/all/my2_1'
    dtpath = '../infer/ablation2/my2_no3'

    gtpaths = sorted(glob.glob(os.path.join(gtpath, '*.txt')))
    if len(gtpaths) == 21727:
        gtpaths = gtpaths[20040:]
    dtpaths = sorted(glob.glob(os.path.join(dtpath, '*.txt')))

    assert len(gtpaths) == len(dtpaths)
    diag = np.sqrt(512 * 512 + 1024 * 1024)
    scores = []
    for i in range(len(gtpaths)):
        gt_path = gtpaths[i]
        dt_path = dtpaths[i]
        with open(gt_path) as f:
            gt_cor = np.array([line.strip().split() for line in f if line.strip()], np.float32)
            gt_cor = np.roll(gt_cor[:, :2], -2 * np.argmin(gt_cor[::2, 0]), 0)
        with open(dt_path) as f:
            dt_cor = np.array([line.strip().split() for line in f if line.strip()], np.float32)
            dt_cor = np.roll(dt_cor[:, :2], -2 * np.argmin(dt_cor[::2, 0]), 0)

        # print(gt_cor.shape, dt_cor.shape)

        dists = euclidean_distances(gt_cor, dt_cor)
        dists = np.min(dists, axis=1)

        dists /= diag
        score = np.mean(dists)
        scores.append(score)
    final_score = np.mean(scores)
    print('final_score', final_score * 100)


def draw_from_gt(scene):
    # scene = 'scene_03199_470726.png'
    parent = '/DATA2/Holistic-3D/Structured3D/train'
    path_img = os.path.join(parent, 'image', '%s.png' % scene[:-4])
    path_label = os.path.join(parent, 'label', '%s.txt' % scene[:-4])
    # path_img = 'C:/Users/mindslab/Desktop/pt/holistic3d/presentation/%s.png' % scene[:-4]
    # path_label = 'C:/Users/mindslab/Desktop/pt/holistic3d/presentation/%s.txt' % scene[:-4]

    img = cv2.imread(path_img)
    if img.shape[2] == 4:
        img = cv2.cvtColor(img, cv2.COLOR_RGBA2RGB)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    H, W, C = img.shape

    with open(path_label) as f:
        cor = np.array([line.strip().split() for line in f if line.strip()], np.float32)
        cor = np.roll(cor[:, :2], -2 * np.argmin(cor[::2, 0]), 0)

    show, skel = skeleton2(img, cor, show_hidden=True)

    print(np.max(np.asarray(show.points), axis=0))

    out_points = np.vstack((np.asarray(show.points), np.asarray(skel.points)))
    out = o3d.geometry.PointCloud()
    out.points = o3d.utility.Vector3dVector(out_points)
    o3d.io.write_point_cloud('%s_gt.pcd' % scene[:-4], out)
    # o3d.visualization.draw_geometries(visualise)


def main():
    path_img = 'data/train/image/scene_00001_906322.png'
    path_label = 'data/train/label/scene_00001_906322.txt'
    # room2(path_img, path_label)
    to3d(path_img, path_label)
    return
    # skeleton(path_img, path_label)
    # find_hidden_points(path_img, path_label)
    img, points_final = key_points(path_img, path_label)
    points_final = points_final[np.lexsort((points_final[:, 1], points_final[:, 0]))]
    get_gt_edges(points_final, img, color=COLORS['BLACK'])
    from PIL import Image
    Image.fromarray(img).save('output/gt/discont.png')
    # new_gt()


if __name__ == '__main__':
    main()
