import os
import numpy as np
from omegaconf import OmegaConf
import random
import tarfile
from tqdm import trange, tqdm
from tensorboardX import SummaryWriter

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader

from model import HorizonNet
from dataset import PanoCorBonDataset
from misc.utils import adjust_learning_rate, save_model, load_trained_model
from inference import inference
from challenge.eval_general import test_general
from indoor_layout_evaluation.metrics import evaluate


def feed_forward(net, x, y_bon, y_cor, device):
    x = x.to(device)
    y_bon = y_bon.to(device)
    y_cor = y_cor.to(device)
    losses = {}

    y_bon_, y_cor_ = net(x)

    losses['bon'] = F.mse_loss(y_bon_, y_bon)
    losses['cor'] = F.binary_cross_entropy_with_logits(y_cor_, y_cor)

    # losses['total'] = 0.5 * losses['bon'] + 1.5 * losses['cor']
    losses['total'] = 1.5 * losses['bon'] + 0.5 * losses['cor']

    return losses


class Recon():
    def __init__(self, conf):
        super(Recon, self).__init__()
        self.conf = conf
        self.device = torch.device('cpu' if conf.no_cuda else 'cuda')
        self.set_seed()
        self.conf.path_ckpt = os.path.join(conf.ckpt, conf.id, str(self.conf.seed))
        os.makedirs(self.conf.path_ckpt, exist_ok=True)
        self.set_loader()
        self.set_model()
        self.set_optim()
        self.init_vars()
        self.set_log()

    def set_seed(self):
        if self.conf.seed is None:
            self.conf.seed = random.randint(1, 100000)  # fix seed
        random.seed(self.conf.seed)
        np.random.seed(self.conf.seed)
        torch.manual_seed(self.conf.seed)
        print('seed', self.conf.seed)

    def set_loader(self):
        # Create dataloader
        self.dataset_train = PanoCorBonDataset(
            root_dir=self.conf.train_root_dir, mode='train',
            flip=not self.conf.no_flip, rotate=not self.conf.no_rotate,
            gamma=not self.conf.no_gamma, stretch=not self.conf.no_pano_stretch)
        self.loader_train = DataLoader(
            self.dataset_train, self.conf.batch_size_train, shuffle=True, drop_last=True,
            num_workers=self.conf.num_workers, pin_memory=not self.conf.no_cuda)
        if self.conf.valid_root_dir:
            self.dataset_valid = PanoCorBonDataset(
                root_dir=self.conf.valid_root_dir, mode='eval', return_cor=True,
                flip=False, rotate=False, gamma=False, stretch=False)

    def load_model(self):
        conf = self.conf
        device = self.device
        if conf.pth is not None:
            self.net = load_trained_model(HorizonNet, conf.pth).to(device)
            print('Finetune model is given.')
            print('Ignore --backbone and --no_rnn')
        else:
            self.net = HorizonNet(conf.backbone, not conf.no_rnn).to(device)
            print('checkpoint loading failed')

    def set_model(self):
        # Create model
        conf = self.conf
        self.load_model()

        assert -1 <= conf.freeze_earlier_blocks and conf.freeze_earlier_blocks <= 4
        if conf.freeze_earlier_blocks != -1:
            b0, b1, b2, b3, b4 = self.net.feature_extractor.list_blocks()
            blocks = [b0, b1, b2, b3, b4]
            for i in range(conf.freeze_earlier_blocks + 1):
                print('Freeze block%d' % i)
                for m in blocks[i]:
                    for param in m.parameters():
                        param.requires_grad = False

        if conf.bn_momentum:
            for m in self.net.modules():
                if isinstance(m, (nn.BatchNorm1d, nn.BatchNorm2d)):
                    m.momentum = conf.bn_momentum

    def set_optim(self):
        # Create optimizer
        conf = self.conf
        if conf.optim == 'SGD':
            self.optim = torch.optim.SGD(
                filter(lambda p: p.requires_grad, self.net.parameters()),
                lr=conf.lr, momentum=conf.beta1, weight_decay=conf.weight_decay)
        elif conf.optim == 'Adam':
            self.optim = torch.optim.Adam(
                filter(lambda p: p.requires_grad, self.net.parameters()),
                lr=conf.lr, betas=(conf.beta1, 0.999), weight_decay=conf.weight_decay)
        else:
            raise NotImplementedError()

    def set_log(self):
        # Create tensorboard for monitoring training
        conf = self.conf
        self.tb_path = os.path.join(conf.logs, conf.id, str(conf.seed))
        os.makedirs(self.tb_path, exist_ok=True)
        self.tb_writer = SummaryWriter(log_dir=self.tb_path)
        with tarfile.open(os.path.join(conf.path_ckpt, 'code.tar.gz'), "w:gz") as tar:
            for addfile in [
                'config_train.yaml', 'train.py', 'model.py', 'dataset.py',
                'inference.py', 'CoordConv.py',
            ]:
                tar.add(addfile)

    def init_vars(self):
        # Init variable
        self.conf.warmup_iters = self.conf.warmup_epochs * len(self.loader_train)
        self.conf.max_iters = self.conf.epochs * len(self.loader_train)
        self.conf.running_lr = self.conf.warmup_lr if self.conf.warmup_epochs > 0 else self.conf.lr
        self.conf.cur_iter = 0
        self.conf.cur_iter_valid = 0
        self.conf.best_valid_score = 0

    def eval_phase(self):
        device = self.device
        self.net.eval()
        if self.conf.valid_root_dir:
            valid_loss = {}
            pbar = tqdm(range(len(self.dataset_valid)), position=2)
            for step in pbar:
                pbar.set_description('Valid ep%d ' % self.conf.cur_epoch)
                x, y_bon, y_cor, gt_cor_id = self.dataset_valid[step]
                x, y_bon, y_cor = x[None], y_bon[None], y_cor[None]
                self.conf.cur_iter_valid += 1

                with torch.no_grad():
                    losses = feed_forward(self.net, x, y_bon, y_cor, device)

                    # True eval result instead of training objective
                    true_eval = dict([
                        (n_corner, {'2DIoU': [], '3DIoU': [], 'rmse': [], 'delta_1': []})
                        for n_corner in ['4', '6', '8', '10+', 'odd', 'overall']
                    ])

                    try:
                        dt_cor_id, z0, z1, vis_out = inference(self.net, x, device, force_cuboid=False)
                        dt_cor_id[:, 0] *= 1024
                        dt_cor_id[:, 1] *= 512
                    except:
                        dt_cor_id = np.array([
                            [k // 2 * 1024, 256 - ((k % 2) * 2 - 1) * 120]
                            for k in range(8)
                        ])
                        dt_cor_id = dt_cor_id[np.lexsort((dt_cor_id[:, 1], dt_cor_id[:, 0]))]

                    test_general(dt_cor_id, gt_cor_id, 1024, 512, true_eval)
                    losses['2DIoU'] = torch.FloatTensor([true_eval['overall']['2DIoU']])
                    losses['3DIoU'] = torch.FloatTensor([true_eval['overall']['3DIoU']])
                    losses['rmse'] = torch.FloatTensor([true_eval['overall']['rmse']])
                    losses['delta_1'] = torch.FloatTensor([true_eval['overall']['delta_1']])

                    THRES = {'junction': [5, 10, 15], 'wireframe': [5, 10, 15], 'plane': 0.5}
                    Fs = evaluate(gt_cor_id.astype(int), dt_cor_id.astype(int), THRES)
                    for key, value in THRES.items():
                        if isinstance(value, list):
                            for i in range(len(value)):
                                num = value[i]
                                losses['%s%d' % (key, num)] = Fs[key][i]
                        elif isinstance(value, int) or isinstance(value, float):
                            losses[key] = Fs[key]

                # write losses every step
                for key, value in losses.items():
                    if not isinstance(value, torch.Tensor):
                        value = torch.as_tensor(value)
                    valid_loss[key] = valid_loss.get(key, 0) + value * x.size(0)
                    if self.conf.cur_iter_valid % (10 * self.conf.print_every):
                        key = 'valid_step/%s' % key
                        try:
                            self.tb_writer.add_scalar(key, value.item(), self.conf.cur_iter_valid)
                        except Exception as e:
                            print('kv', key, value)
                            raise e

            # write average loss for each epoch
            for key, value in valid_loss.items():
                if not isinstance(value, torch.Tensor):
                    value = torch.as_tensor(value)
                key = 'valid/%s' % key
                self.tb_writer.add_scalar(key, value.item() / len(self.dataset_valid), self.conf.cur_epoch)

            # Save best validation loss model
            now_valid_score = valid_loss['3DIoU'] / len(self.dataset_valid)
            if isinstance(now_valid_score, torch.Tensor):
                now_valid_score = now_valid_score.item()
            '''pbar.set_description(
                'Valid ep %d cur_score %.4f best %.4f' % (
                    self.conf.cur_epoch, now_valid_score, self.conf.best_valid_score))'''
            print('Ep%3d %.4f vs. Best %.4f' % (self.conf.cur_epoch, now_valid_score, self.conf.best_valid_score))
            # TODO print를 쓸 거면 tqdm을 왜 씀?
            if now_valid_score > self.conf.best_valid_score:
                self.conf.best_valid_score = now_valid_score
                save_model(self.net, os.path.join(self.conf.path_ckpt, 'best_valid.pth'), self.conf)

    def train_phase(self):
        device = self.device
        self.net.train()
        if self.conf.freeze_earlier_blocks != -1:
            b0, b1, b2, b3, b4 = self.net.feature_extractor.list_blocks()
            blocks = [b0, b1, b2, b3, b4]
            for i in range(self.conf.freeze_earlier_blocks + 1):
                for m in blocks[i]:
                    m.eval()
        iterator_train = iter(self.loader_train)
        for step in trange(len(self.loader_train),
                           desc='Train ep%s' % self.conf.cur_epoch, position=1):
            # Set learning rate
            adjust_learning_rate(self.optim, self.conf)
            self.conf.cur_iter += 1

            x, y_bon, y_cor = next(iterator_train)
            losses = feed_forward(self.net, x, y_bon, y_cor, device)

            if self.conf.cur_iter % self.conf.print_every == 0:
                for key, value in losses.items():
                    key = 'train/%s' % key
                    self.tb_writer.add_scalar(key, value.item(), self.conf.cur_iter)
                self.tb_writer.add_scalar('train/lr', self.conf.running_lr, self.conf.cur_iter)
            loss = losses['total']

            # backprop
            self.optim.zero_grad()
            loss.backward()
            nn.utils.clip_grad_norm_(self.net.parameters(), 3.0, norm_type='inf')
            self.optim.step()

    def train(self):
        # Start training
        for ith_epoch in trange(1, self.conf.epochs + 1, desc='Epoch', unit='ep'):
            self.conf.cur_epoch = ith_epoch
            # Train phase
            self.train_phase()

            # Valid phase
            self.eval_phase()

            # Periodically save model
            if ith_epoch % self.conf.save_every == 0:
                save_model(self.net, os.path.join(self.conf.path_ckpt, 'epoch_%d.pth' % ith_epoch), self.conf)


def main():
    conf = OmegaConf.load('configs/config_train.yaml')
    R = Recon(conf)
    R.train()

    return


if __name__ == '__main__':
    main()
