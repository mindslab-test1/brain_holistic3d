# HorizonNet

## Requirements
`requirements.txt`
- opencv-python>=3.1 (for pre-processing) (also can't be too new, the latest opencv removed a key algorithm due to patent, 3.1.0.5 works. )
- open3d>=0.7 (for layout 3D viewer)
- shapely

## Docker Build
```
docker build --no-cache -f Dockerfile-t docker.maum.ai:443/brain/holistic3d:v1.0.2 .
```

```
docker run -itd --ipc=host --gpus='"device=1"' -p 39995:39995 --name room3d_server docker.maum.ai:443/brain/holistic3d:v1.0.2
```

## Reminder

* Should be pre-processed(aligned)
* will return .pcd file


## Citation
Please cite our paper for any purpose of usage.
```
@inproceedings{SunHSC19,
  author    = {Cheng Sun and
               Chi{-}Wei Hsiao and
               Min Sun and
               Hwann{-}Tzong Chen},
  title     = {HorizonNet: Learning Room Layout With 1D Representation and Pano Stretch
               Data Augmentation},
  booktitle = {{IEEE} Conference on Computer Vision and Pattern Recognition, {CVPR}
               2019, Long Beach, CA, USA, June 16-20, 2019},
  pages     = {1047--1056},
  year      = {2019},
}
```

